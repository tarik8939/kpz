using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using lab6.Models;

namespace lab6.Controllers
{
    public class SecondController : Controller
    {
        private string connstr = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=DB;Integrated Security=True";
        private Context db = new Context();
        public ActionResult Index()
        {
            return View(db.Drivers.ToList());
        }

        // GET: Second/Details/5
        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }
        // GET: Second/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Drivers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DriverId,DriverFirstName,DriverLastName,Email,PhoneNumber,Gender")] Driver driver)
        {
            if (ModelState.IsValid)
            {
                 var conn = new SqlConnection(connstr);
                conn.Open();
                var CommandText = $"insert into Drivers values('{driver.DriverFirstName}','{driver.DriverLastName}'," +
                                  $"'{driver.Email}','{driver.PhoneNumber}','{driver.Gender}')";
                SqlCommand command = new SqlCommand(CommandText, conn);
                command.ExecuteNonQuery();
                return RedirectToAction("Index");
            }

            return View(driver);
        }
        // GET: Second/Edit/5
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        // POST: Second/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DriverId,DriverFirstName,DriverLastName,Email,PhoneNumber,Gender")] Driver driver)
        {
            if (ModelState.IsValid)
            {
                var conn = new SqlConnection(connstr);
                conn.Open();
                var CommandText = $"update Drivers set DriverFirstName='{driver.DriverFirstName}'," +
                                  $"DriverLastName='{driver.DriverLastName}'," +
                                  $"Email='{driver.Email}',PhoneNumber='{driver.PhoneNumber}',Gender='{driver.Gender}'" +
                                  $"where DriverId={driver.DriverId}";
                SqlCommand command = new SqlCommand(CommandText, conn);
                command.ExecuteNonQuery();
                return RedirectToAction("Index");
            }
            return View(driver);
        }
        
        // GET: Second/Delete/5
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        // POST: Second/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var conn = new SqlConnection(connstr);
            conn.Open();
            var CommandText =$"delete from Drivers where DriverId ={id}" ;
            SqlCommand command = new SqlCommand(CommandText, conn);
            command.ExecuteNonQuery();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



       
    }
}