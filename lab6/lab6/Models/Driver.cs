using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace lab6.Models
{
    public class Driver
    {
        [HiddenInput(DisplayValue = false)]
        public int DriverId { get; set; }

        [Required(ErrorMessage = "The field 'Name' must be set")]
        [Display(Name = "Name")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Too long field")]
        // [RegularExpression(@"/^[a-zA-Z'][a-zA-Z-' ]+[a-zA-Z']?$/", ErrorMessage = "Invalid Name")]
        public string DriverFirstName { get; set; }

        [Required(ErrorMessage = "The field 'Surname' must be set")]
        [Display(Name = "Surname")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Too long field")]
        // [RegularExpression(@"/^[a-zA-Z'][a-zA-Z-' ]+[a-zA-Z']?$/", ErrorMessage = "Invalid Surname")]
        public string DriverLastName { get; set; }

        [Required(ErrorMessage = "The field 'Email' must be set")]
        [Display(Name = "Email")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Too long field")]
        // [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "Invalid Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The field 'Phone number' must be set")]
        [Display(Name = "Phone number")]
        [StringLength(10, MinimumLength = 3, ErrorMessage = "Too long field")]
        // [RegularExpression(@"^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$", ErrorMessage = "Invalid Phone number")]
        public string PhoneNumber { get; set; }

        // [HiddenInput(DisplayValue = false)]
        public string Gender { get; set; }

    }
}