﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taras.Horilchanyi.RobotChallange
{
    public class HorilchanyiAlgorithm : IRobotAlgorithm
    {
        //public string Author => throw new NotImplementedException();
        public int robotCount = 10;
        public string Author
        {
            get { return "Taras Horilchanyi"; }
        }

        public string Description
        {
            //get { throw new NotImplementedException(); } 
            get
            {
                return
                    String.Concat("Demo");
            }
        }
        public int RoundCount { get; set; }
        public HorilchanyiAlgorithm()
        {
            Logger.OnLogMessage += Logger_OnLogMessage;
        }

        private void Logger_OnLogMessage(object sender, LogEventArgs e)
        {
            RoundCount++;
        }

        public bool IsEnemy(Robot.Common.Robot movingRobot, Robot.Common.Robot unknownRobot)
        {
            if (movingRobot.OwnerName == unknownRobot.OwnerName)
                return false;
            else return true;
        }

        public bool IsMovingRobotIsCollector(Robot.Common.Robot movingRobot, Map map)
        {
            foreach (var station in map.Stations)
            {
                if (Math.Abs(station.Position.X - movingRobot.Position.X) <= 2 &&
               Math.Abs(station.Position.Y - movingRobot.Position.Y) <= 2)
                {
                    if (station.Energy > 0)
                        return true;
                }
            }
            return false;
        }

        public List<Robot.Common.Robot> RobotsOnStation(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            List<Robot.Common.Robot> robotsUsedStation = new List<Robot.Common.Robot>();
            for (int i = station.Position.X - 1; i <= station.Position.X + 1; ++i)
                for (int j = station.Position.Y - 1; j <= station.Position.Y + 1; ++j)
                {
                    Robot.Common.Robot robotCell = RobotOnCell(new Position(i, j), movingRobot, robots);
                    if (robotCell != null)
                        robotsUsedStation.Add(robotCell);
                }
            return robotsUsedStation;
        }

        public Robot.Common.Robot RobotOnCell(Position cell, Robot.Common.Robot movingRobot,IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot.Position != movingRobot.Position)
                {
                    if (robot.Position == cell)
                        return robot;
                }
            }
            return null;
        }
        //last test


        public EnergyStation FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {

                EnergyStation nearestFreeStation = null;
                int minDistanceToFreeStation = int.MaxValue;
                foreach (var station in map.Stations)
                {
                    List<Robot.Common.Robot> robotsOnStation = RobotsOnStation(station, movingRobot,
                   robots);
                    int temp = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (robotsOnStation.Count == 0)
                    {
                        if (temp < minDistanceToFreeStation)
                        {
                            minDistanceToFreeStation = temp;
                            nearestFreeStation = station;
                        }
                    }
                }
                return nearestFreeStation;


        }
        //
        public Robot.Common.Robot FindNearestRobotNearStation(Robot.Common.Robot movingRobot,IList<Robot.Common.Robot> robots)
        {

                int minDistance = int.MaxValue;
                Robot.Common.Robot attackRobot = null;
                foreach (var robot in robots)
                {
                    if (IsEnemy(movingRobot, robot) && robot != movingRobot)
                    {
                        int temp = DistanceHelper.FindDistance(robot.Position, movingRobot.Position);
                        if ((minDistance > temp))
                        {
                            attackRobot = robot;
                            minDistance = temp;
                        }
                    }
                }
                return attackRobot;

        }
        public Position FindBestPositionToMove(Position positionStation, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robot)
        {
                int bestX = positionStation.X;
                int bestY = positionStation.Y;
                int minDistance = int.MaxValue;
                for (int i = positionStation.X - 1; i <= positionStation.X + 1; ++i)
                    for (int j = positionStation.Y - 1; j <= positionStation.Y + 1; ++j)
                    {
                        int temp = DistanceHelper.FindDistance(new Position(i, j),
                       movingRobot.Position);
                        Robot.Common.Robot robotC = RobotOnCell(new Position(i, j), movingRobot,
                       robot);
                        if (temp < minDistance && robotC == null && i < 100 & j < 100 && i > 0 && j > 0)
                        {
                            minDistance = temp;
                            bestX = i;
                            bestY = j;
                        }
                    }
                return new Position(bestX, bestY);

        }

        public Position FindBestChoose(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
                int collectEnergy = 0;
                int EnergyGetAttack = 0;
                int EnergyGetFree = 0;
                if (IsMovingRobotIsCollector(movingRobot, map))
                {
                    collectEnergy = 75;
                }
                EnergyStation stationFree = FindNearestFreeStation(movingRobot, map, robots);
                if (stationFree != null)
                {
                    EnergyGetFree = -DistanceHelper.FindDistance(stationFree.Position,
                   movingRobot.Position);
                }
                Robot.Common.Robot robotAttack = FindNearestRobotNearStation(movingRobot, robots);
                if (robotAttack != null)
                {
                    EnergyGetAttack = robotAttack.Energy - DistanceHelper.FindDistance(robotAttack.Position, movingRobot.Position);
                }
                if (EnergyGetFree <= 0 && EnergyGetAttack <= 0 && collectEnergy == 0)
                    return BetterWayToMove(FindBestPositionToMove(stationFree.Position, movingRobot,
                   robots), movingRobot, robots);
                if (collectEnergy > 0)
                {
                    if (collectEnergy >= EnergyGetAttack)
                        return null;
                    else
                        return robotAttack.Position;
                }
                if (EnergyGetAttack > EnergyGetFree)
                    return robotAttack.Position;
                else
                    return BetterWayToMove(FindBestPositionToMove(stationFree.Position, movingRobot, robots), movingRobot, robots);

            
        }

        public Position BetterWayToMove(Position stationPosition, Robot.Common.Robot movingRobot,IList<Robot.Common.Robot> robots)
        {

                if (DistanceHelper.FindDistance(stationPosition, movingRobot.Position) < movingRobot.Energy)
                    return stationPosition;
                int tempX = 0;
                int tempY = 0;
                int divider = 2;
                while (divider <= 10)
                {
                    tempX = movingRobot.Position.X + (stationPosition.X - movingRobot.Position.X) /
                   divider;
                    tempY = movingRobot.Position.Y + (stationPosition.Y - movingRobot.Position.Y) /
                   divider;
                    int tempDistance = DistanceHelper.FindDistance(new Position(tempX, tempY),
                   movingRobot.Position);
                    Robot.Common.Robot robotCell = RobotOnCell(new Position(tempX, tempY), movingRobot,
                   robots);
                    if (tempDistance * divider <= movingRobot.Energy && robotCell == null)
                        return new Position(tempX, tempY);
                    else
                        ++divider;
                }
                //чи вільно
                return new Position(movingRobot.Position.X + 1, movingRobot.Position.Y + 1);


        }
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {

                Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
                if ((movingRobot.Energy > 500) && (RoundCount < 50) && (robotCount < 100))
                {
                    robotCount++;
                    //movingRobot.Energy -= 50;
                    return new CreateNewRobotCommand() { };
                }
                Position stationPosition = FindBestChoose(robots[robotToMoveIndex], map, robots);
                if (stationPosition == null)
                    return new CollectEnergyCommand();
                else
                {
                    return new MoveCommand() { NewPosition = stationPosition };
                }
            


        }










    }
}
