﻿using System;
using Robot.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Taras.Horilchanyi.RobotChallange.Test
{
    [TestClass]
    public class TestAlgorythm
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            var algorithm = new HorilchanyiAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                    { new Robot.Common.Robot()
                    {Energy = 500,Position = new Position(2,3)}};
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsFalse(command is MoveCommand);
        }

        [TestMethod]
        public void TestCollectCommand()
        {
            var algorithm = new HorilchanyiAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                    { new Robot.Common.Robot()
                    {Energy = 200,Position = new Position(1,1)}};
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);
        }

        [TestMethod]
        public void TestCreateNewRobot()
        {
            var algorithm = new HorilchanyiAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                    { new Robot.Common.Robot()
                    {Energy = 600,Position = new Position(2,3)}};
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestIsEnemy()
        {
            var algorithm = new HorilchanyiAlgorithm();
            var r1 = new Robot.Common.Robot() { OwnerName = "Taras" };
            var r2 = new Robot.Common.Robot() { OwnerName = "No Taras" };
            Assert.AreNotEqual(r1.OwnerName, r2.OwnerName);
        }
        [TestMethod]
        public void TestIsCollector()
        {
            var algorithm = new HorilchanyiAlgorithm();
            var map = new Map();
            var robot = new Robot.Common.Robot() { Position = new Position(2, 2) };
            var stationPosition = new Position(4, 4);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var rez = algorithm.IsMovingRobotIsCollector(robot, map);
            Assert.IsTrue(rez is true);
        }
        [TestMethod]
        public void RobotOnCell()
        {
            var algorithm = new HorilchanyiAlgorithm();
            var cell = new Position() { X=1,Y=1 };
            var robot = new Robot.Common.Robot() { Position = new Position(2, 2) };
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Position = new Position(1, 1) } };
            var rez = algorithm.RobotOnCell(cell, robot,robots);
            Assert.AreEqual(rez, robots[0]);

        }

        [TestMethod]
        public void RobotsOnStation()
        {
            var algorithm = new HorilchanyiAlgorithm();
            var robot = new Robot.Common.Robot() { Position = new Position(2, 2) };
            var robotsUsedStation = new List<Robot.Common.Robot>();
            var station = new EnergyStation() {Position = new Position(2,2) };



        }
        [TestMethod]
        public void NearestFreeStation()
        {
            var alg = new HorilchanyiAlgorithm();
            var map = new Map();
            var stationPos1 = new Position(10, 10);
            var stationPos2 = new Position(20, 20);
            
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = stationPos1,
                RecoveryRate = 2
             });
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = stationPos2,
                RecoveryRate = 2
            });
            Owner Enemy = new Owner();
            Owner Friend = new Owner();
            Enemy.Name = "Enemy";
            Friend.Name = "My";
            var movingRobotPos = new Position(15, 15);
            var enemyRobotPos = new Position(19, 19);
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot() { Energy=400,Position = enemyRobotPos, OwnerName = Enemy.Name},new Robot.Common.Robot
            {Energy=400, Position= movingRobotPos, OwnerName = Friend.Name} };
            var station = alg.FindNearestFreeStation(robots[1], map, robots);
            Assert.AreEqual(station.Position, stationPos1);
        }
        [TestMethod]
        public void FindNearestRobotNearStation() 
        {
            var alg = new HorilchanyiAlgorithm();
            Robot.Common.Robot attackRobot = null;
            var robot = new Robot.Common.Robot() { Position = new Position(2, 2) };
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Position = new Position(1, 1) } };
            var command = alg.FindNearestRobotNearStation(robot, robots);
            Assert.AreEqual(command,attackRobot);
        }

    }
}
