using System.Windows.Controls;

namespace lab5.View
{
    public partial class AddNewUserControl : UserControl
    {
        public AddNewUserControl()
        {
            InitializeComponent();
            DataContext = new AddNewViewModel();
        }
    }
}