﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using lab5.Annotations;
using lab5.View;


namespace lab5
{
    public class ApplicationViewModel : BaseViewModel
    {
        private Route selectedRoute;
        private Command addCommand;
        private Command removeCommand;
        public Command SetControlVisibility { get; set; }

        private string _visibleControl = "Start";
        public ObservableCollection<Route> Routes { get; set; }



        public ApplicationViewModel()
        {
            Routes = new ObservableCollection<Route>
            {
                new Route {StartPoint="NYC", EndPoint="Miami", Price=250, StartDate = new DateTime(2020,10,14), EndDate = new DateTime(2020,10,15)},
                new Route {StartPoint="Boston", EndPoint="Atlanta", Price =300, StartDate = new DateTime(2020,10,4), EndDate = new DateTime(2020,10,5) },
                new Route {StartPoint="Miami", EndPoint="Boston", Price=650, StartDate = new DateTime(2020,10,7), EndDate = new DateTime(2020,10,9) },
                new Route {StartPoint="Atlanta", EndPoint="Orlando", Price=400, StartDate = new DateTime(2020,10,14), EndDate = new DateTime(2020,10,6) },
                new Route {StartPoint="Orlando", EndPoint="Miami", Price=400, StartDate = new DateTime(2020,10,14), EndDate = new DateTime(2020,10,14) },
            };
            
            Messenger.Default.Register<Route>(this, (NewRoute) =>
            {
                AddCommand.Execute(NewRoute);
            });
            SetControlVisibility = new Command(ControlVisibility);
            
        }
        public string VisibleControl
        {
            get
            {
                return _visibleControl;
            }
            set
            {
                _visibleControl = value;
                OnPropertyChanged("VisibleControl");
            }
        }

        public void ControlVisibility(object args)
        {
            VisibleControl = args.ToString();
        }
        
        public Command AddCommand
        {
            get
            {
                return addCommand ??
                       (addCommand = new Command(obj =>
                       {
                           Route route = obj as Route;
                           Routes.Insert(0, route); 
                       }));
            }
        }
        public Command RemoveCommand
        {
            get
            {
                return removeCommand ??
                       (removeCommand = new Command(obj => 
                       {
                           Route route = obj as Route;
                           if (route != null)
                           {
                               Routes.Remove(route);
                           }
                       },(obj) => SelectedRoute != null));
            }
        }
        
        public Route SelectedRoute
        {
            get { return selectedRoute; }
            set
            {
                selectedRoute = value;
                OnPropertyChanged("SelectedRoute");
            }
        }
    }
}
