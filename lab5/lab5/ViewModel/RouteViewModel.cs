using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace lab5
{
    //під питанням
    public class RouteViewModel : INotifyPropertyChanged
    {
    private Route route;

    public RouteViewModel(Route r)
    {
        route = r;
    }



    public string StartPoint
    {
        get { return route.StartPoint; }
        set
        {
            route.StartPoint = value;
            OnPropertyChanged("StartPoint");
        }
    }

    public string EndPoint
    {
        get { return route.EndPoint; }
        set
        {
            route.EndPoint = value;
            OnPropertyChanged("EndPoint");
        }
    }

    public int Price
    {
        get { return route.Price; }
        set
        {
            route.Price = value;
            OnPropertyChanged("Price");
        }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
    }
}