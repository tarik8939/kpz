using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using lab5.Annotations;

namespace lab5
{
    public class AddNewViewModel : BaseViewModel
    {
        private Route route;
        
        private Command create;
        private Command close;
        
        public AddNewViewModel()
        {
            Route = new Route();
        }
        
        public Command Create
        {
            get
            {
                return create ??
                       (create = new Command(obj =>
                       {
                           Route r = obj as Route;
                           if (r.Price == null)
                           {
                               MessageBox.Show("Write price");
                           }
                           else if (r.StartPoint == null)
                           {
                               MessageBox.Show("Write start point");
                           }
                           else if (r.EndPoint == null)
                           {
                               MessageBox.Show("Write end point");
                           }
                           else if (r.StartDate == null)
                           {
                               MessageBox.Show("Write start date");
                           }
                           else if (r.EndDate == null)
                           {
                               MessageBox.Show("Write end date");
                           }
                           else
                           {
                               Messenger.Default.Send(r);    
                           }
                       }));
            }
        }
        
        public Command Close
        {
            get
            {
                return close ??
                       (close = new Command(obj =>
                       {
                           Application.Current.Shutdown();
                       }));
            }
        }
        public Route Route
        {
            get { return route; }
            set
            {
                route = value;
                OnPropertyChanged("Route");
            }
        }
    }
}