﻿namespace Lab3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.terminal = new System.Windows.Forms.RichTextBox();
            this.WriteBTN = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.PriceBTN = new System.Windows.Forms.Button();
            this.CategoryBTN = new System.Windows.Forms.Button();
            this.cmpBTN = new System.Windows.Forms.Button();
            this.SortBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // terminal
            // 
            this.terminal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.terminal.Location = new System.Drawing.Point(410, 12);
            this.terminal.Name = "terminal";
            this.terminal.Size = new System.Drawing.Size(466, 515);
            this.terminal.TabIndex = 0;
            this.terminal.Text = "";
            // 
            // WriteBTN
            // 
            this.WriteBTN.Location = new System.Drawing.Point(12, 12);
            this.WriteBTN.Name = "WriteBTN";
            this.WriteBTN.Size = new System.Drawing.Size(113, 39);
            this.WriteBTN.TabIndex = 1;
            this.WriteBTN.Text = "Write";
            this.WriteBTN.UseVisualStyleBackColor = true;
            this.WriteBTN.Click += new System.EventHandler(this.WriteBTN_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(245, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 2;
            // 
            // PriceBTN
            // 
            this.PriceBTN.Location = new System.Drawing.Point(146, 12);
            this.PriceBTN.Name = "PriceBTN";
            this.PriceBTN.Size = new System.Drawing.Size(93, 39);
            this.PriceBTN.TabIndex = 3;
            this.PriceBTN.Text = "Where ";
            this.PriceBTN.UseVisualStyleBackColor = true;
            this.PriceBTN.Click += new System.EventHandler(this.PriceBTN_Click);
            // 
            // CategoryBTN
            // 
            this.CategoryBTN.Location = new System.Drawing.Point(12, 68);
            this.CategoryBTN.Name = "CategoryBTN";
            this.CategoryBTN.Size = new System.Drawing.Size(113, 41);
            this.CategoryBTN.TabIndex = 4;
            this.CategoryBTN.Text = "All category";
            this.CategoryBTN.UseVisualStyleBackColor = true;
            this.CategoryBTN.Click += new System.EventHandler(this.CategoryBTN_Click);
            // 
            // cmpBTN
            // 
            this.cmpBTN.Location = new System.Drawing.Point(146, 68);
            this.cmpBTN.Name = "cmpBTN";
            this.cmpBTN.Size = new System.Drawing.Size(93, 41);
            this.cmpBTN.TabIndex = 5;
            this.cmpBTN.Text = "Compare";
            this.cmpBTN.UseVisualStyleBackColor = true;
            this.cmpBTN.Click += new System.EventHandler(this.cmpBTN_Click);
            // 
            // SortBTN
            // 
            this.SortBTN.Location = new System.Drawing.Point(245, 68);
            this.SortBTN.Name = "SortBTN";
            this.SortBTN.Size = new System.Drawing.Size(100, 41);
            this.SortBTN.TabIndex = 6;
            this.SortBTN.Text = "Sort by name";
            this.SortBTN.UseVisualStyleBackColor = true;
            this.SortBTN.Click += new System.EventHandler(this.SortBTN_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 539);
            this.Controls.Add(this.SortBTN);
            this.Controls.Add(this.cmpBTN);
            this.Controls.Add(this.CategoryBTN);
            this.Controls.Add(this.PriceBTN);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.WriteBTN);
            this.Controls.Add(this.terminal);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.Button CategoryBTN;
        private System.Windows.Forms.Button cmpBTN;
        private System.Windows.Forms.Button PriceBTN;
        private System.Windows.Forms.Button SortBTN;
        private System.Windows.Forms.RichTextBox terminal;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button WriteBTN;

        #endregion
    }
}