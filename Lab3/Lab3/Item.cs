using System;

namespace Lab3
{
    public class Item
    {
        public String Name { get; set; }
        public int Price { get; set; }

        public Brand Brand { get; set; }

        public Item(string name, int price, Brand brand)
        {
            Name = name;
            Price = price;
            Brand = brand;
        }

        public override string ToString()
        {
            return $"Name:{Name}, price:{Price}, brand:{Brand.BrandName}";
        }
    }
}