using System;

namespace Lab3
{
    public class Brand
    {
        public String BrandName { get; set; }
        public int BrandId { get; set; }

        public Brand(string brandName, int brandId)
        {
            BrandName = brandName;
            BrandId = brandId;
        }



        public override string ToString()
        {
            return $"Brand-{BrandName}";
        }
    }
}