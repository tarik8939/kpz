﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Lab3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void WriteBTN_Click(object sender, EventArgs e)
        {
            var i = 0;
            var items = DBMock.Items;
            var newlist = items.Select(x => $"{++i}) {x.ToString()}").ToArray();
            terminal.Text = String.Join(Environment.NewLine, newlist);
        }

        private void PriceBTN_Click(object sender, EventArgs e)
        {
            var i = 0;
            var price = Convert.ToInt32(textBox1.Text);
            var items = DBMock.Items;
             var newlist = items.Where(x => x.Price >= price).
                 Select(x => $"{++i}) {x.ToString()}").ToArray();
             terminal.Text = String.Join(Environment.NewLine, newlist);
             
        }

        private void CategoryBTN_Click(object sender, EventArgs e)
        {
            var i = 0;
            //var categories = DBMock.Categories;
            var items = DBMock.Items;
            
            // var asd = from item in items
            //     group item by item.Category.CategoryName;
            // var line = default(String);
            // foreach (IGrouping<String,Item> g in asd)
            // {
            //     line += $"{g.Key}:";
            //     foreach (var item in g)
            //     {
            //         line += $"{item.Name} ";
            //     }
            // }
            // MessageBox.Show(line);
            var dict = items.GroupBy(x => x.Brand.BrandName).
                ToDictionary(x => x.Key, x => x.Count());
            // or
            // var phoneGroups = items.GroupBy(p => p.Brand.BrandName)
            //     .Select(g => new {Name = g.Key, Count = g.Count()});

            //var newlist = categories.Select(x => $"{++i}) {x.ToString()}");
            var line = dict.Select(x => $"{x.Key}: {x.Value}").ToArray();
            terminal.Text = String.Join(Environment.NewLine, line);

        }
        private void cmpBTN_Click(object sender, EventArgs e)
        {
            var list = DBMock.Items;
            list.Sort(Comparer<Item>.Create((i1,i2)=>i1.Name.Length-i2.Name.Length));
            terminal.Text = String.Join(Environment.NewLine, list);
        }

        private void SortBTN_Click(object sender, EventArgs e)
        {
            var cat = DBMock.Categories;
            var items = DBMock.Items.OrderBy(x => x.Name);
            terminal.Text = String.Join(Environment.NewLine, items);
            MessageBox.Show("1323s3".Count123().ToString());
            var rez = from item in items
                join C in cat on item.Brand.BrandId equals C.BrandId
                select new {ID = C.BrandId, BrandName = item.Brand.BrandName};
        }
    }
}