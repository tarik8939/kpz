using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Lab3
{
    static class DBMock
    {
        public static List<Brand> Categories = new List<Brand>()
        {
            new Brand(brandName:"Microsoft",0),
            new Brand(brandName:"Xiaomi",1),
            new Brand(brandName:"Apple",2),
            new Brand(brandName:"LG",3)
        };

        public static List<Item> Items = new List<Item>()
        {
            new Item(name:"Lumia 430",300,new Brand(brandName:"Microsoft",0)),
            new Item(name:"Lumia 930",600,new Brand(brandName:"Microsoft",0)),
            new Item(name:"Lumia 630",350,new Brand(brandName:"Microsoft",0)),
            new Item(name:"Mi 5",700,new Brand(brandName:"Xiaomi",1)),
            new Item(name:"Redmi Note 9 Pro",900,new Brand(brandName:"Xiaomi",1)),
            new Item(name:"Redmi Note 9 ",770,new Brand(brandName:"Xiaomi",1)),
            new Item(name:"iPhone X",800,new Brand(brandName:"Apple",2)),
            new Item(name:"iPhone XR",1000,new Brand(brandName:"Apple",2)),
            new Item(name:"iPhone XR Max",1150,new Brand(brandName:"Apple",2)),
            new Item(name:"LG G 4",375,new Brand(brandName:"LG",3)),
            new Item(name:"LG G 3",350,new Brand(brandName:"LG",3)),
            
        };
    }
}