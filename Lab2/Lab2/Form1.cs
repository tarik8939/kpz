﻿using System;
using System.Windows.Forms;

namespace Lab2
{
    public partial class Form1 : Form
    {
        public Manager Manager;
        
        public Form1()
        {
            InitializeComponent();
            Manager = new Manager();
            Manager.AddNotify += AddMessage;
            Manager.DeleteNotify += DellMessage;
            Manager.BuyNotify += BuyMessage;
            dataGridView1.Rows.Add("item1",15.5);
            dataGridView1.Rows.Add("item2",12.75);
            dataGridView1.Rows.Add("item3",45.25);
            dataGridView1.Rows.Add("item4",20.20);
            dataGridView1.Rows.Add("item5",31.0);
        }
        

        private void Add_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentCell.RowIndex;
            var Name = (String) dataGridView1.Rows[index].Cells[0].Value;
            var Price = Convert.ToDecimal(dataGridView1.Rows[index].Cells[1].Value);
            var item = new Item(Name, Price);
            Manager.AddToList(item);
            
        }
        
        private void Delete_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentCell.RowIndex;
            var Name = (String) dataGridView1.Rows[index].Cells[0].Value;
            var Price = Convert.ToDecimal(dataGridView1.Rows[index].Cells[1].Value) ;
            var item = new Item( Name, Price);
            Manager.DelFromList(item);
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dataGridView1.CurrentCell.RowIndex;
            var Name = (String) dataGridView1.Rows[index].Cells[0].Value;
            var Price = Convert.ToDecimal(dataGridView1.Rows[index].Cells[1].Value) ;
            var item = new Item( Name, Price);
            Manager.AddToList(item);

            //Manager.Handler(new Manager.Handler(AddMessage));
            //MessageBox.Show(Convert.ToString(item.GetPrice()));


        }

        private  void AddMessage(object sender, ManagerEventArgs e)
        {
            terminal.AppendText($"{e.date}:{e.Message}");

        }
        private  void DellMessage(object sender, ManagerEventArgs e)
        {
            terminal.Clear();
            terminal.AppendText($"{e.date}:{e.Message}");
            foreach (var VARIABLE in Manager.List)
            {
                terminal.AppendText(VARIABLE.ToString());
            }
        }

        private void BuyMessage(object sender, ManagerEventArgs e)
        {
            //terminal.Clear();
            terminal.AppendText($"{e.date}:{e.Message} = {e.sum}");
        }


        private void Buy_Click(object sender, EventArgs e)
        {
            Manager.Buy();
        }
    }
}