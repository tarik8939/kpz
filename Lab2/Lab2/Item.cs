using System;

namespace Lab2
{
    public class Item
    {
        private String Name { get; set; }
        private decimal Price{ get; set; }
        public Item(string name, decimal price)
        {
            Name = name;
            Price = price;
        }
        public String GetName()
        {
            return Name;
        }
        public decimal GetPrice()
        {
            return Price;
        }
        public void  SetName(String name)
        {
            Name = name;
        }
        public void SetPrice(decimal price)
        {
            Price = price;
        }

        public override string ToString()
        {
            return $"Name: {this.GetName()} Price: {this.GetPrice()}\n";
        }
    }
}