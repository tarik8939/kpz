using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab2
{
    public  class Manager
    {
        
        public  List<Item> List;

        public delegate void Handler(object sender, ManagerEventArgs e);

        public event Handler AddNotify;
        
        public event Handler DeleteNotify;
        
        public event Handler BuyNotify;
        public Manager()
        {
            List = new List<Item>();
        }

        public String asd()
        {
            foreach (var VARIABLE in List)
            {
                return VARIABLE.ToString();
            }

            return "\n";
        }

        public void AddToList(Item item)
        {
            List.Add(item);
            //AddNotify?.Invoke($"Added to list {item.ToString()}",);
            AddNotify?.Invoke(item,
                new ManagerEventArgs($"Added to list {item.ToString()}",DateTime.Now));
        }
        public void DelFromList(Item item)
        {
            List.RemoveAt(FindLast(item));
            //DeleteNotify?.Invoke($"Remove from list {item.ToString()}New list:\n");
            DeleteNotify?.Invoke(item,
                new ManagerEventArgs($"Remove from list {item.ToString()}New list:\n",DateTime.Now));
            
        }

        private int FindLast(Item item)
        {
            int index = 0;
            for (int i = 0; i < List.Count-1; i++)
            {
                if (List[i].GetName() == item.GetName())
                {
                    index = i;
                }
            }
            return index;
        }

        public void Buy()
        {
            decimal sum = 0;
            foreach (var VARIABLE in List)
            {
                sum += VARIABLE.GetPrice();
            }
            BuyNotify?.Invoke(null,
                new ManagerEventArgs($"Summary",DateTime.Now,sum));
            
        }
        
    }
}