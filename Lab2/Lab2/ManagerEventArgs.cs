using System;

namespace Lab2
{
    public class ManagerEventArgs
    {
        public String Message { get; }
        public DateTime date { get; }
        public decimal sum { get; }

        public ManagerEventArgs(string message, DateTime date)
        {
            Message = message;
            this.date = date;
        }

        public ManagerEventArgs(string message, DateTime date, decimal sum)
        {
            Message = message;
            this.date = date;
            this.sum = sum;
        }
    }
}