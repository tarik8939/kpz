using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace lab7.Models
{
    public class Client
    {
        [HiddenInput(DisplayValue = false)]
        public int ClientId { get; set; }

        [Required(ErrorMessage = "The field 'Name' must be set")]
        [Display(Name = "Name")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Too short field")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "The field 'Surname' must be set")]
        [Display(Name = "Surname")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Too short field")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "The field 'Email' must be set")]
        [Display(Name = "Email")]
        [StringLength(50, MinimumLength = 10, ErrorMessage = "Too short field")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The field 'Password' must be set")]
        [Display(Name = "Password")]
        [StringLength(50, MinimumLength = 10, ErrorMessage = "Too short field")]
        public string Password { get; set; }

        [Required(ErrorMessage = "The field 'Phone number' must be set")]
        [Display(Name = "Phone number")]
        [StringLength(10, MinimumLength = 3, ErrorMessage = "Too short field")]
        public string PhoneNumber { get; set; }
        public string Gender { get; set; }

        [Required(ErrorMessage = "The field 'Age' must be set")]
        [Display(Name = "Age")]
        [Range(1, 100, ErrorMessage = "The field 'Age' must be in the range between 1 and 100")]
        public int Age { get; set; }
    }
}