using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace lab7.Models
{
    public class Driver
    {
        [HiddenInput(DisplayValue = false)]
        public int DriverId { get; set; }

        [Required(ErrorMessage = "The field 'Name' must be set")]
        [Display(Name = "Name")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Too short field")]
        public string DriverFirstName { get; set; }

        [Required(ErrorMessage = "The field 'Surname' must be set")]
        [Display(Name = "Surname")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Too short field")]
        public string DriverLastName { get; set; }

        [Required(ErrorMessage = "The field 'Email' must be set")]
        [Display(Name = "Email")]
        [StringLength(50, MinimumLength = 10, ErrorMessage = "Too short field")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The field 'Phone number' must be set")]
        [Display(Name = "Phone number")]
        [StringLength(10, MinimumLength = 3, ErrorMessage = "Too short field")]
        public string PhoneNumber { get; set; }

        public string Gender { get; set; }
    }
}