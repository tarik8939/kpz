using Microsoft.EntityFrameworkCore;

namespace lab7.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {
            Database.EnsureCreated();   
        }
        
        public DbSet<Driver> Drivers { get; set; }

        public DbSet<Client> Clients { get; set; }
    }
}