﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            //2
            // var Driver = new Driver();
            // Driver.Word(); //public
            // Driver.Drive(); //private
            // Driver.Exist(); //protected
            // Driver.DoDifMethod(); //protected internal
            // Driver.Hello(); //internal
            // Driver.StopWorking();//private protected
            
            Console.WriteLine();
            var client = new Client();
            client.AllRoute = new List<Route>()
            {
                new Route() {price = 25},
                new Route() {price = 35},
                new Route() {price = 50}
            };
            int sum;
            int sum2 = 0;
            Manager.Calc(ref client.AllRoute, out sum);
            Console.WriteLine($" All tickets {sum}");
            Manager.Calc2(client.AllRoute, sum2);
            Console.WriteLine($" All tickets {sum2}");
            Console.WriteLine($"\n\n{Manager.CalcAvg(client.AllRoute)}");
            
            var elem1 = AutoCategory.Car1 | AutoCategory.Car3 | AutoCategory.Car4;
            var elem2 = AutoCategory.Car1 | AutoCategory.Car2 | AutoCategory.Car3 | AutoCategory.Car4;
            Console.WriteLine(elem1);
            Console.WriteLine(elem2);
            Console.WriteLine((elem1 & elem2));
            Console.ReadKey();
        }

    }
}
