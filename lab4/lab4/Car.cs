﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Car
    {

        public int number { get; set; }
        public int capacity { get; set; }

        public void Move()
        {
            Console.WriteLine($"Car number {number} moving");
        }
        public void Move(int km)
        {
            Console.WriteLine($"Car number {number} riding {km}");
        }
        public Car(int num, int cap) : this(num)
        {
            //number = num;
            capacity = cap;
        }
        public Car(int num)
        {
            number = num;
        }

        public Car()
        {
  
        }
    }
}
