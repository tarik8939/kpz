﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Driver : Worker, IHuman
    {
        //2 Завдання
        public string Name 
        {
            get 
            {
                return Name;
            }
            set 
            {
                Name = value;
            }
        }
        public string LastName 
        {
            get
            {
                return LastName;
            }
            set
            {
                LastName = value;
            }
        }
        protected string Email { get; set; }
        internal class PassengerAuto : Car { }
        public void Word()
        {
            Console.WriteLine("Driver is working");
        }
        private void Drive()
        {
            Console.WriteLine("I'm driving");
        }
        protected void Exist()
        {
            Console.WriteLine($"Hello, i'm  exist");
        }
        protected internal static void DoDifMethod()
        {
            var D = new Driver();
            var M = new PassengerAuto();
            M.Move();
            D.Word();
            D.Drive();
            D.Exist();
        }
        internal void Hello()
        {
            Console.WriteLine("Hello");
        }
        private protected void StopWorking()
        {
            Console.WriteLine("Stop working");
        }
    }
}
