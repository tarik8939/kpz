﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Client: IHuman
    {
        public string Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }
        public string LastName
        {
            get
            {
                return LastName;
            }
            set
            {
                LastName = value;
            }
        }
        public List<Route> AllRoute;

        int Balance { get; set; }
        void GetBalance()
        {
            Console.WriteLine($"Client {Name} balance-{Balance}");
        }

    }
}
