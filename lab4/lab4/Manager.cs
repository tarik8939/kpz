﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Manager
    {
        public static void Calc(ref List<Route> list, out int sum)
        {
            //int sum =0;
            sum = 0;
            foreach (var item in list)
            {
                sum += item.price;
            }
        }
        public static void Calc2(List<Route> list, int sum)
        {
            //int sum =0;
            //sum = 0;
            foreach (var item in list)
            {
                sum += item.price;
            }
        }
        public static int Calc3(List<Route> list)
        {
            int sum =0;
            List<object> newlist = new List<object>();
            list.ForEach(x => newlist.Add(x.price));//boxing
            foreach (var item in newlist)
            {
                sum += (int)item;//unboxing
            }
            return sum;
        }
        public static double CalcAvg(List<Route> list)
        {
            double sum = 0;
            foreach (var item in list)
            {
                sum += (double)item.price;
            }
            return sum / list.Count;
        }
    }
}
