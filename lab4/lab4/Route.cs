﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    internal class Route//internal by default
    {
        private Car car { get; set; } = new Car() { number = 452578 };
        public int price;
        public Route(int p)
        {
            price = p;
        }

        public Route()
        {
        }

        protected internal void number()
        {
            Console.WriteLine($"{car.GetNum()}");
        }


        private class Car
        {
            public int number { get; set; }
            public int GetNum()
            {
                return number;
            }

        }
    }
}
