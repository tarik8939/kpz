﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class PassengerAuto : Car
    {
        //public int number
        //{
        //    get
        //    {
        //        return number;
        //    }
        //    set
        //    {
        //        number = value;
        //    }
        //}
        //public int capacity
        //{
        //    get
        //    {
        //        return capacity;
        //    }
        //    set
        //    {
        //        capacity = value;
        //    }
        //}
        //public void Move()
        //{
        //    Console.WriteLine($"Car number {number} moving");
        //}
        public int RouteId { get; set; }
        public int CarId { get; set; }
        public static int FuelConsumption = 0;
        public PassengerAuto(int num, int cap) : base(num, cap)
        {

        }
        public PassengerAuto(int num, int cap, int id, int id2) : base(num, cap)
        {
            RouteId = id;
            CarId = id2;
        }

        static PassengerAuto()
        {
            FuelConsumption = 20;
        }

    }
}
