﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    [Flags]
    enum AutoCategory
    {
        Car1 = 1,
        Car2 = 2,
        Car3 = 4,
        Car4 = 8
    }
}
